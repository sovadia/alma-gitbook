# Resources Training

This is a potential landing spot for Alma training materials.

## Videos

Alma Resource Management Videos Recommended for CUNY

1.	[Navigating in Alma](https://knowledge.exlibrisgroup.com/Alma/Training/Alma_Essentials/Alma_Essentials_-_English/B_Navigation_and_Searching/01_Navigating_in_Alma):  For cataloging supervisors

2.	[Search in Alma](https://knowledge.exlibrisgroup.com/Alma/Training/Alma_Essentials/Alma_Essentials_-_English/B_Navigation_and_Searching/02_Searching_in_Alma): For all catalogers  

3.	[Local Search Indexes and External Search Resources](https://knowledge.exlibrisgroup.com/Alma/Training/Alma_Essentials/Alma_Essentials_-_English/E_Resource_Management/03_Local_Search_Indexes_and_External_Search_Resources) (5 min): For cataloging supervisors 

4.	[Metadata Editor and Cataloging Templates](https://knowledge.exlibrisgroup.com/Alma/Training/Alma_Essentials/Alma_Essentials_-_English/E_Resource_Management/02_Metadata_Editor_and_Cataloging_Templates_(19_min)) (19 min): For all catalogers  

5.	[Shared Bibliographic Records in the Network Zone](https://knowledge.exlibrisgroup.com/Alma/Training/Alma_Collaborative_Networks_%28Alma_Consortia%29/A__Fundamentals/02_Shared_Bibliographic_Records_in_the_Network_Zone): For all catalogers  

6.	[Cataloging Activities in the Network Zone](https://knowledge.exlibrisgroup.com/Alma/Training/Alma_Collaborative_Networks_%28Alma_Consortia%29/C_Cataloging) (2 parts): For cataloging supervisors  

7.	[Getting to know Alma, Session 3 Resource Management](https://proquestmeetings.webex.com/mw3300/mywebex/default.do?nomenu=true&siteurl=proquestmeetings&service=6&rnd=0.9392421815086762&main_url=https://proquestmeetings.webex.com/ec3300/eventcenter/program/programDetail.do?theAction%3Ddetail%26internalProgramTicket%3D4832534b000000049998276b077be468ccb70e4b3b8186d3b49b8ccb9657f559143b0d67d1434e7d%26siteurl%3Dproquestmeetings%26internalProgramTicketUnList%3D4832534b000000049998276b077be468ccb70e4b3b8186d3b49b8ccb9657f559143b0d67d1434e7d%26cProgViewID%3D1451432%26PRID%3D8efd9300896f0a4c507bd73798d56388): For cataloging supervisors, once the sandbox is available  

8.	SUNY videos (for all catalogers) – Need to be aware that the new metadata editor interface will be available in January 2020.

9.	[SUNY Alma cataloging FAQs](http://slcny.libanswers.com/search/?t=0&adv=1&topics=Cataloging): For all catalogers  

10.	[CUNY Alma Glossary with Crosswalks @CUNY](https://www.youtube.com/watch?v=PPAGw6zdyWo&feature=youtu.be); [Also here](https://guides.cuny.edu/ld.php?content_id=50095170)

11.	Harvard – Training videos (?) are password protected. Has training wiki 